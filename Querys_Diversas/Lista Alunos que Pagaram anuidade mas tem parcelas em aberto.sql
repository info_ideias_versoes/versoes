select matricula, coalesce(primnomealuno,'')||' '||coalesce(segnomealuno,'')||' '||coalesce(ultnomealuno,'') as nome
  from aluno
 where exists(select 1
                from alunoparcela
               where alunoparcela.matricula =  aluno.matricula
                 and alunoparcela.anoletivo = '2017 '
                 and alunoparcela.tipocarneparcela = 'M'
                 and alunoparcela.parcela = 13
                 and ((alunoparcela.statusparcela = 'Q') or
                      (alunoparcela.statusparcela = 'N' and 
                       alunoparcela.valparcpendente <> alunoparcela.valorparcela )
                       )
                )       
  and  exists(select 1
                from alunoparcela
               where alunoparcela.matricula =  aluno.matricula
                 and alunoparcela.anoletivo = '2017 '
                 and alunoparcela.tipocarneparcela = 'M'
                 and alunoparcela.parcela >= 1
                 and alunoparcela.parcela <= 12
                 and alunoparcela.statusparcela = 'N' )                