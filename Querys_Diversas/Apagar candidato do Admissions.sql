DELETE
  FROM adm_escolas_anteriores
 WHERE candidato_id IN (SELECT id
                          FROM adm_candidatos
                         WHERE email_responsavel_aplicacao = :email_responsavel_aplicacao:
                          AND application_status = 1);
DELETE
  FROM adm_candidatos_futuros
WHERE candidato_id IN (SELECT id
                         FROM adm_candidatos
                        WHERE email_responsavel_aplicacao = :email_responsavel_aplicacao:
                          AND application_status = 1);						  
DELETE
  FROM adm_candidato_familias
WHERE candidato_id IN (SELECT id
                         FROM adm_candidatos
                        WHERE email_responsavel_aplicacao = :email_responsavel_aplicacao:
                          AND application_status = 1);

DELETE
  FROM adm_candidato_status
 WHERE candidato_id IN (SELECT id
                          FROM adm_candidatos
                         WHERE email_responsavel_aplicacao = :email_responsavel_aplicacao:
                           AND application_status = 1);


DELETE
  FROM adm_candidatos
 WHERE email_responsavel_aplicacao = :email_responsavel_aplicacao:
   AND application_status = 1;     						  