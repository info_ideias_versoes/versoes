select matricula, parcela, TIPOCARNEPARCELA, valorparcela, datavencimento,valorpendente, situacao
  from (
        select  parcelas.matricula, parcelas.PARCELA,parcelas.TIPOCARNEPARCELA,parcelas.VALORPARCELA,parcelas.DATAVENCIMENTO,parcelas.valorpendente, 
                case when ctrl_regristro.matricula is null then  'N�o registrado' 
                     when parcelas.VALORPARCELA <> ctrl_regristro.VALORDOCUMENTOCONFIRMADOBANCO then 'Valor registrado diferente do documento. Valor registrado: '||ctrl_regristro.VALORDOCUMENTOCONFIRMADOBANCO 
                     when parcelas.DATAVENCIMENTO <> ctrl_regristro.DATAVENCIMENTOCONFIRMADOBANCO then 'Vencimento registrado diferente do documento. Vencimento: '||ctrl_regristro.DATAVENCIMENTOCONFIRMADOBANCO       
                     when ctrl_regristro.statuscontroleregistro = 'E' then 'Enviado, mas ainda sem retorno'
                     when ctrl_regristro.statuscontroleregistro = 'C' then 'Cr�tica: '||coalesce((select critica
                                                                                                    from ARQUIVOCOBRANCALINHA
                                                                                                   where ARQUIVOCOBRANCALINHA.controleregistroid = ctrl_regristro.idCONTROLEREGISTRO
                                                                                                     and rownum = 1),'') 
                     end situacao
          from vw_parcelas_aluno parcelas
          left join (select matricula,parcela ,anoletivo,numerobanco,
                            CONTROLEREGISTRO.VALORDOCUMENTOCONFIRMADOBANCO, 
                            CONTROLEREGISTRO.DATAVENCIMENTOCONFIRMADOBANCO,
                            CONTROLEREGISTRO.statuscontroleregistro,
                            CONTROLEREGISTRO.idCONTROLEREGISTRO,
                            controleregistro.tipocarneparcela,
                            CONTROLEREGISTRO.IDALUNOTAXA,
                            CONTROLEREGISTRO.NOSSONUMERO
                       from controleregistro
                      where numerobanco in ( select max(controleregistro.numerobanco) as numerobanco
                                               from controleregistro
                                              group by controleregistro.NOSSONUMERO) )  ctrl_regristro  
            on (ctrl_regristro.NOSSONUMERO = PARCELAS.NUMERODOCUMENTO ) 
         where parcelas.anoletivo = '2017 '
           and PARCELAS.STATUSPARCELA = 'N'
           and coalesce(parcelas.registrar,'N') = 'S' )y
where situacao is not null   
order by matricula, parcela