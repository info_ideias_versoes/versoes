CREATE VIEW VW_PARCELAS_ALUNO
AS select anoletivo, tipocarneparcela, parcela, matricula, coalesce(DATAVENCIMENTOREGISTRO, datavencimento) as datavencimento,
       coalesce(VALORREGISTRO, VALORPARCELA) as VALORPARCELA, valparcpendente as valorpendente,
       statusparcela , registrar,NOSSONUMERO, NUMERODOCUMENTO ,
       case when statusparcela = 'Q' then 'Quitada'
            when statusparcela = 'A' then 'Abonada'
            when statusparcela = 'N' then 'Aberta' end status,
       null as idalunotaxa     
  from alunoparcela
 union all  
select anoletivo, 'T' as tipocarneparcela, ALUNOTAXAPARCELA.PARCELATAXA as parcela, matricula,
       ALUNOTAXAPARCELA.DATAVENCIMENTO ,  ALUNOTAXAPARCELA.VALORLOCALPARCELA as valorparcela, 
       ALUNOTAXAPARCELA.VALORINTERPENDENTE as valorpendente,
       ALUNOTAXAPARCELA.STATUSTAXAPARCELA as statusparcela, alunotaxaparcela.registrar,
       alunotaxaparcela.nossonumero, ALUNOTAXAPARCELA.NUMERODOCUMENTO,
       case when ALUNOTAXAPARCELA.STATUSTAXAPARCELA = 'Q' then 'Quitada'
            when ALUNOTAXAPARCELA.STATUSTAXAPARCELA = 'A' then 'Abonada'
            when ALUNOTAXAPARCELA.STATUSTAXAPARCELA = 'N' then 'Aberta' end status,
       alunotaxa.idalunotaxa     
  from alunotaxa
 inner join alunotaxaparcela  
    on (alunotaxa.idalunotaxa = alunotaxaparcela.idalunotaxa);
