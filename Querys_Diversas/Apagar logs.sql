delete
  from logcampo
 where exists(select 1
                from logtabela
               where logtabela.idlogtabela = logcampo.idlogtabela
                 and logtabela.datalogtabela <= '31/12/2016' );
commit;
               
delete
  from logtabela
 where logtabela.datalogtabela <= '31/12/2016';               
 
commit; 